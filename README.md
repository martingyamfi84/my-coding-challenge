**How to execute the solutions**
1.	I created CI/CD pipeline for the java file.
2.	In the java file are test parameters to test the methods.
3.	Edit the parameters the way it suit’s you.
4.	Commit your changes.
5.	The pipeline will load automatically.
6.	Click on the pipeline icon.
7.	You will notice two icons build and execute.
8.	Click on the execute icon you will find the output from the solutions.

**My sample code**
The code checks whether a string is a valid password. Below are the password rules
1.	A password must have at least ten characters.
2.	A password consists of only letters and digits.
3.	A password must contain at least two digits.
