import java.util.ArrayList;
import java.util.Arrays;

public class Interview_Challenge {
	
	public static final int PASSWORD_LENGTH = 8;


	public static void main(String[] args) {
	
	  // testing Four_Number_Sum method
		  int[] quad = { 7, 6, 4, -1, 1, 2 };
	      int sum = 16;
	        
	        System.out.println("Quadruplets: " + Four_Number_Sum(quad,sum)); 
	        
	        
	     // testing Smallest_Difference method 
	        int A[] = {1, 2, 11, 5}; 
	        int B[] = {4, 12, 19, 23, 127, 235}; 
	      
			 System.out.println("minimum difference between two array elements: is between " + Smallest_Difference(A,B)); 
			 
	        
	       // testing isPalindrome method
			 String word = "abcdcba";
			 System.out.println("isPalindrome: " + isPalindrome(word)); 
			 
			 
			 //testing print3largest method
			 System.out.println("three largest numbers: " + print3largest(A)); 
			 
	        
	      
	        //testing password validation
			 String password = "works123";
	        if (is_Valid_Password(password)) {
	            System.out.println("Password is valid: " + password);
	        } else {
	            System.out.println("Not a valid password: " + password);
	        }


	}
	
	
	
	 private static ArrayList<ArrayList<Integer>> Four_Number_Sum(int[] array, int sum) {
	        ArrayList<ArrayList<Integer>> quad_array = new ArrayList<>();
	        Arrays.sort(array);

	        if (array.length == 0) {
	            System.out.println("Please enter a non-empty array");
	        } else {
	            for (int i = 0; i <= array.length - 4; i++) {
	                for (int j = i + 1; j <= array.length - 3; j++) {
	                    int k = sum - (array[i] + array[j]);
	                    int low = j + 1, high = array.length - 1;

	                    while (low < high) {
	                        if (array[low] + array[high] < k) {
	                            low++;
	                        } else if (array[low] + array[high] > k) {
	                            high--;
	                        } else {
	                            quad_array.add(new ArrayList<>(Arrays.asList(array[i], array[j], array[low], array[high])));
	                            low++;
	                            high--;
	                        }
	                    }
	                }
	            }

	        }

	        return quad_array;
	    }

	
	 
	 private static ArrayList<Integer> Smallest_Difference(int a1[], int a2[]){
		 ArrayList<Integer> result = new ArrayList<Integer>();
		int minDiff = Integer.MAX_VALUE;
		int min1 = -1;
		int min2 = -1;
		int i = 0;
		int j = 0;
		int n1 = a1.length;
		int n2 = a2.length;
		int diff = 0;
		
		if(n1 == 0 || n2 == 0){
			
		 System.out.println("Please enter a non-empty array");
			
		}else{
			Arrays.sort(a1);
			Arrays.sort(a2);
			while(i < n1 && j < n2){
				diff = Math.abs(a1[i]-a2[j]);
				if(diff < minDiff){
					minDiff = diff;
					min1 = a1[i];
					min2 = a2[j];
				}
				
				if(a1[i] < a2[j]){
					i++;
				}
				else{
					j++;
				}
			}
			
			result.add(min1);
			result.add(min2);
		}
		
		return result;
	}
	 
	 
	 
	 
	 private static boolean isPalindrome(String str) { 
	     
		  if(str.length()!=0){
			  int i = 0, j = str.length() - 1; 
		        while (i < j) { 
		            if (str.charAt(i) != str.charAt(j)) 
		                return false;
		            i++; 
		            j--;  
		        } 
		        return true;      
		  }
		return false;
	           
	    } 
	 
	 
	 
	 public static ArrayList<Integer> print3largest(int arr[]){
			int i, first, second, third; 
			  
	        if (arr.length < 3) { 
	            System.out.print(" Invalid Input "); 
	         
	        } 
	        
	        third = first = second = Integer.MIN_VALUE; 
	        for (i = 0; i < arr.length; i++) { 
	          
	            if (arr[i] > first) { 
	                third = second; 
	                second = first; 
	                first = arr[i]; 
	            } 
	
	            else if (arr[i] > second) { 
	                third = second; 
	                second = arr[i]; 
	            } 
	  
	            else if (arr[i] > third) 
	                third = arr[i]; 
	         
	        }

	        ArrayList<Integer> result = new ArrayList<Integer>();
	       result.add(third);
	       result.add(second);
	       result.add(first);
	      
	       return result; 
		}

	 
	 
	    
	    
	    
	    private static boolean is_Valid_Password(String password) {

	        if (password.length() < PASSWORD_LENGTH) return false;

	        int charCount = 0;
	        int numCount = 0;
	        for (int i = 0; i < password.length(); i++) {

	            char ch = password.charAt(i);

	            if (is_Numeric(ch)) numCount++;
	            else if (is_Letter(ch)) charCount++;
	            else return false;
	        }


	        return (charCount >= 2 && numCount >= 2);
	    }

	    private static boolean is_Letter(char ch) {
	        ch = Character.toUpperCase(ch);
	        return (ch >= 'A' && ch <= 'Z');
	    }


	    private static boolean is_Numeric(char ch) {

	        return (ch >= '0' && ch <= '9');
	    }


}
